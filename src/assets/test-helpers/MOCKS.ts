import { Category } from "src/app/models/category";
import { Product } from "src/app/models/product";
import { Supplier } from "src/app/models/supplier";


export class MOCKS {
    static SUPPLIER_MOCK: Supplier = {
        supplierId: '1',
        companyName: 'Supplier name'
    }

    static SUPPLIER_LIST_MOCK: Supplier[] = [{
        supplierId: '1',
        companyName: 'Supplier name'
    },
    {
        supplierId: '2',
        companyName: 'Supplier name 2'
    }]

    static CATEGORY_MOCK: Category = {
        categoryId: '1',
        categoryName: 'category name'
    }

    static CATEGORY_LIST_MOCK: Category[] = [{
        categoryId: '1',
        categoryName: 'category name'
    },
    {
        categoryId: '2',
        categoryName: 'category name 2'
    }]

    static PRODUCT_MOCK: Product = {
        Id: 'abcdefghijklmnopqrstuvwxyz',
        productId: '0',
        productName: 'Product Zero',
        supplierId: '2',
        categoryId: '2',
        quantityPerUnit: '0',
        unitPrice: 0,
        unitsInStock: 0,
        unitsInOrder: 0,
        reorderLevel: 0,
        discontinued: 0
    }

     static PRODUCT_LIST_MOCK: Product [] = [{
        Id: 'abcdefghijklmnopqrstuvwxyz',
        productId: '1',
        productName: 'Product One',
        supplierId: '1',
        categoryId: '1',
        quantityPerUnit: '0',
        unitPrice: 0,
        unitsInStock: 0,
        unitsInOrder: 0,
        reorderLevel: 0,
        discontinued: 0
    }, 
    {
        Id: 'abcdefghijklmnopqrstuvwxyz',
        productId: '2',
        productName: 'Product Two',
        supplierId: '2',
        categoryId: '2',
        quantityPerUnit: '0',
        unitPrice: 0,
        unitsInStock: 0,
        unitsInOrder: 0,
        reorderLevel: 0,
        discontinued: 0
    },
    {
        Id: 'abcdefghijklmnopqrstuvwxyz',
        productId: '3',
        productName: 'Product Three',
        supplierId: '2',
        categoryId: '1',
        quantityPerUnit: '0',
        unitPrice: 0,
        unitsInStock: 0,
        unitsInOrder: 0,
        reorderLevel: 0,
        discontinued: 0
    }]
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly apiURL = environment.apiBase;

  constructor(private http: HttpClient) { }

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.apiURL.concat('/product'));
  }

  getProductByCategoryId(categoryId: number): Observable<Product[]> {
    if (categoryId.toString() === '0') {
      return this.http.get<Product[]>(this.apiURL.concat('/product'));
    } else {
      return this.http.get<Product[]>(this.apiURL.concat('/product/category/').concat(categoryId.toString()));
    }
  }

  getProductBySupplierId(supplierId: number): Observable<Product[]> {
    if (supplierId.toString() === '0') {
      return this.http.get<Product[]>(this.apiURL.concat('/product'));
    } else {
      return this.http.get<Product[]>(this.apiURL.concat('/product/supplier/').concat(supplierId.toString()));
    }
  }

  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.apiURL.concat('/product/update'), product);
  }
}

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MOCKS } from 'src/assets/test-helpers/MOCKS';
import { environment } from 'src/environments/environment';
import { Supplier } from '../models/supplier';

import { SupplierService } from './supplier.service';

describe('SupplierService', () => {
  let service: SupplierService;
  let httpMock: HttpTestingController;
  let supplier: Supplier;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SupplierService);

    supplier = new Supplier();

    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should use GET method to get categories', () => {

    service.getSuppliers().subscribe(data => {
      expect(data).toBeTruthy();
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/supplier'));

    expect(request.request.method).toBe('GET')

    expect(service).toBeTruthy();
  });

  it('should get suppliers', () => {

    service.getSuppliers().subscribe(data => {
      expect(data.length).toBeGreaterThan(1);
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/supplier'));

    request.flush(MOCKS.SUPPLIER_LIST_MOCK);
  });
});

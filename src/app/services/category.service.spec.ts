import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CategoryService } from './category.service';
import { Category } from '../models/category';
import { environment } from 'src/environments/environment';
import { MOCKS } from 'src/assets/test-helpers/MOCKS';

describe('CategoryService', () => {
  let service: CategoryService;
  let httpMock: HttpTestingController;
  let category: Category;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CategoryService);
    category = new Category();

    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should use get method to get categories', () => {

    service.getCategories().subscribe(data => {
      expect(data).toBeTruthy();
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/category'));

    expect(request.request.method).toBe('GET')

    expect(service).toBeTruthy();
  });

  it('should get categories', () => {

    service.getCategories().subscribe(data => {
      expect(data.length).toBeGreaterThan(1);
    });
    const request = httpMock.expectOne(environment.apiBase.concat('/category'));

    request.flush(MOCKS.CATEGORY_LIST_MOCK);
  });
});

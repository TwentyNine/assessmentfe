import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CategoryService } from './category.service';
import { Category } from '../models/category';
import { environment } from 'src/environments/environment';
import { MOCKS } from 'src/assets/test-helpers/MOCKS';
import { ProductService } from './product.service';
import { Product } from '../models/product';

describe('ProductService', () => {
  let service: ProductService;
  let httpMock: HttpTestingController;
  let product: Product;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ProductService);
    product = new Product();

    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call products with GET method and return with product', () => {
    service.getAllProducts().subscribe(data => {
      expect(data.length).toEqual(3)
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/product'));

    expect(request.request.method).toBe('GET')

    request.flush(MOCKS.PRODUCT_LIST_MOCK);
  });

  it('should get all products if there is no suplier id', () => {
    service.getProductBySupplierId(0).subscribe(data => {
      expect(data.length).toEqual(3)
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/product'));

    request.flush(MOCKS.PRODUCT_LIST_MOCK);
  });

  it('should get all products based on suplier id', () => {
    let supplierId = 1;

    service.getProductBySupplierId(supplierId).subscribe(data => {
      expect(data.length).toEqual(1);
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/product/supplier/').concat(supplierId.toString()));

    request.flush(MOCKS.PRODUCT_LIST_MOCK.filter(prod => prod.supplierId === supplierId.toString()));
  });

  it('should get no products based on suplier id', () => {
    let supplierId = 14;

    service.getProductBySupplierId(supplierId).subscribe(data => {
      expect(data.length).toEqual(0);
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/product/supplier/').concat(supplierId.toString()));

    request.flush(MOCKS.PRODUCT_LIST_MOCK.filter(prod => prod.supplierId === supplierId.toString()));
  });

  it('should get all products based on category id', () => {
    let categoryId = 1;

    service.getProductByCategoryId(categoryId).subscribe(data => {
      expect(data.length).toEqual(2);
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/product/category/').concat(categoryId.toString()));

    request.flush(MOCKS.PRODUCT_LIST_MOCK.filter(prod => prod.categoryId === categoryId.toString()));
  });

  it('should get no products based on category id', () => {
    let categoryId = 10;

    service.getProductByCategoryId(categoryId).subscribe(data => {
      expect(data.length).toEqual(0);
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/product/category/').concat(categoryId.toString()));

    request.flush(MOCKS.PRODUCT_LIST_MOCK.filter(prod => prod.categoryId === categoryId.toString()));
  });

  it('should update product', () => {
    
     let PRODUCT_MOCK = {
      Id: 'abcdefghijklmnopqrstuvwxyz',
      productId: '0',
      productName: 'Product updated',
      supplierId: '2',
      categoryId: '2',
      quantityPerUnit: '0',
      unitPrice: 0,
      unitsInStock: 0,
      unitsInOrder: 0,
      reorderLevel: 0,
      discontinued: 0
  }

    service.updateProduct(MOCKS.PRODUCT_MOCK).subscribe(data => {
      expect(data).toEqual(PRODUCT_MOCK);
    });

    const request = httpMock.expectOne(environment.apiBase.concat('/product/update'));
    expect(request.request.method).toBe('PUT')

    request.flush(PRODUCT_MOCK);
  });
});

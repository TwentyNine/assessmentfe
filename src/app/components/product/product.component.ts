import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
import { SupplierService } from 'src/app/services/supplier.service';
import { Supplier } from 'src/app/models/supplier';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap'; 


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  spin: boolean;
  products: Product[];
  categories: Category[];
  suppliers: Supplier[];
  productForm: FormGroup;
  editProductForm: FormGroup;
  selectCategoryForm: FormGroup;
  selectSupplierForm: FormGroup;


  @ViewChild('successModal', {static: true}) successModal: TemplateRef<any>;
  @ViewChild('failureModal', {static: true}) failureModal: TemplateRef<any>;

  closeResult: string;

  successModalMessage: string;
  failureModalMessage: string;

  showTable: boolean = true;
  tempId: string = '0';
  ObjectTempId: string = '0';

  constructor(public productService: ProductService, private formBuilder: FormBuilder,
    public categoryService: CategoryService, public supplierService: SupplierService, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.spin = true;
    this.createForm();
    this.getProducts();
    this.loadCategories();
    this.loadSuppliers();
  }

  createForm(): void {

    this.productForm = this.formBuilder.group({
      productId: [''],
      productName: [''],
      supplierId: [''],
      categoryId: [''],
      quantityPerUnit: [''],
      unitPrice: [''],
      unitsInStock: [''],
      unitsInOrder: [''],
      reorderLevel: [''],
      discontinued: ['']
    });

    this.selectCategoryForm = this.formBuilder.group({
      selectCategory: ['']
    });

    this.selectSupplierForm = this.formBuilder.group({
      selectSupplier: ['']
    });

    this.editProductForm = this.formBuilder.group({
      productId: [''],
      productName: [''],
      supplierId: [''],
      categoryId: [''],
      quantityPerUnit: [''],
      unitPrice: [''],
      unitsInStock: [''],
      unitsInOrder: [''],
      reorderLevel: [''],
      discontinued: ['']
    });

  }

  loadCategories(): void {
    this.categoryService.getCategories().subscribe(res => {
      this.categories = res;
      this.spin = false;
    }, error => {
      this.spin = false;
    });
  }

  loadSuppliers(): void {
    this.supplierService.getSuppliers().subscribe(res => {
      this.suppliers = res;
      this.spin = false;
    }, error => {
      this.spin = false;
    });
  }

  getProducts(): void {
    this.spin = true;

    this.productService.getAllProducts().subscribe(res => {
      this.products = res;

      this.spin = false;

    }, err => {
      this.spin = false;
      this.showFailureModal('An error happened during product load');
    });
  }

  cancelEdit() {
    this.editProductForm.reset();
    this.showTable = true;
  }

  getProductsByCategoryId(categoryId: number): void {
    this.spin = true;

    this.productService.getProductByCategoryId(categoryId).subscribe(res => {
      this.products = res;
    }, err => {
      this.showFailureModal('An error happened during product load');
    });
    this.spin = false;
    this.selectSupplierForm.reset();
  }

  getProductsBySupplierId(supplierId: number): void {
    this.spin = true;

    this.productService.getProductBySupplierId(supplierId).subscribe(res => {
      this.products = res;
    }, err => {
      this.showFailureModal('An error happened during product load');
    });
    this.spin = false;
    this.selectCategoryForm.reset();
  }

  enableEditProduct(product): void {

    this.showTable = false;

    this.ObjectTempId = product.id;
    this.tempId = product.productId;

    this.editProductForm.controls.productName.setValue(product.productName);
    this.editProductForm.controls.categoryId.patchValue(product.categoryId);
    this.editProductForm.controls.supplierId.patchValue(product.supplierId);
    this.editProductForm.controls.quantityPerUnit.setValue(product.quantityPerUnit);
    this.editProductForm.controls.unitPrice.setValue(product.unitPrice);
    this.editProductForm.controls.unitsInStock.setValue(product.unitsInStock);
    this.editProductForm.controls.unitsInOrder.setValue(product.unitsInOrder);
    this.editProductForm.controls.reorderLevel.setValue(product.reorderLevel);
    this.editProductForm.controls.discontinued.setValue(product.discontinued);

  }

  saveEdit(): void {
    this.spin = true;

    let product = {
      Id: this.ObjectTempId,
      productId: this.tempId,
      productName: this.editProductForm.controls.productName.value,
      categoryId: this.editProductForm.controls.categoryId.value,
      supplierId: this.editProductForm.controls.supplierId.value,
      quantityPerUnit: this.editProductForm.controls.quantityPerUnit.value,
      unitPrice: this.editProductForm.controls.unitPrice.value,
      unitsInStock: this.editProductForm.controls.unitsInStock.value,
      unitsInOrder: this.editProductForm.controls.unitsInOrder.value,
      reorderLevel: this.editProductForm.controls.reorderLevel.value,
      discontinued: this.editProductForm.controls.discontinued.value
    };
 
    this.productService.updateProduct(product).subscribe(res => {
      if (res) {
        this.showSuccessModal('This product was successfully updated');
        this.showTable = true;
        this.getProducts();
      } else {
        this.showFailureModal('This product could not be updated');
      }
      this.spin = false;

    }, error => {
      this.showFailureModal('An error happened during product update');
      this.spin = false;
    });
  }

  showSuccessModal(message: string): void {
    this.open(this.successModal);
    this.successModalMessage = message;
  }

  showFailureModal(message: string): void {
    this.open(this.failureModal);
    this.failureModalMessage = message;
  }

  open(content): void {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}

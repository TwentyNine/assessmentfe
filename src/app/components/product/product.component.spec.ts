import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, observable, throwError } from 'rxjs';
import { ProductService } from 'src/app/services/product.service';
import { MOCKS } from 'src/assets/test-helpers/MOCKS';

import { ProductComponent } from './product.component';

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule, RouterTestingModule],
      providers: [ProductService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load categories', () => {

    spyOn(component.categoryService, 'getCategories').and.callFake(() => {
      return Observable.create((observer) => {
        return observer.next(MOCKS.CATEGORY_LIST_MOCK);
      });
    });

    component.loadCategories();
    expect(component.categories).not.toBeNull();
    expect(component.categories.length).toEqual(2);

  });

  it('should load suppliers', () => {

    spyOn(component.supplierService, 'getSuppliers').and.callFake(() => {
      return Observable.create((observer) => {
        return observer.next(MOCKS.SUPPLIER_LIST_MOCK);
      });
    });

    component.loadSuppliers();
    expect(component.suppliers).not.toBeNull();
    expect(component.suppliers.length).toEqual(2);

  });

  it('should load all products', () => {

    spyOn(component.productService, 'getAllProducts').and.callFake(() => {
      return Observable.create((observer) => {
        return observer.next(MOCKS.PRODUCT_LIST_MOCK);
      });
    });

    component.getProducts();
    expect(component.products).not.toBeNull();

  });

  it('should load all products by category', () => {
    let categoryId = 2;

    spyOn(component.productService, 'getProductByCategoryId').and.callFake(() => {
      return Observable.create((observer) => {
        return observer.next(MOCKS.PRODUCT_LIST_MOCK.filter(prod => prod.categoryId === categoryId.toString())).not.toBeNull();
      });
    });

    component.getProductsByCategoryId(categoryId);
    expect(component.products.length).toEqual(1);

  });

  it('should load no products if category does not exist', () => {
    let categoryId = 24;

    spyOn(component.productService, 'getProductByCategoryId').and.callFake(() => {
      return Observable.create((observer) => {
        return observer.next(MOCKS.PRODUCT_LIST_MOCK.filter(prod => prod.categoryId === categoryId.toString())).not.toBeNull();
      });
    });

    component.getProductsByCategoryId(categoryId);
    expect(component.products.length).toEqual(0);

  });

  it('should load all products by supplier', () => {
    let supplierId = 1;

    spyOn(component.productService, 'getProductBySupplierId').and.callFake(() => {
      return Observable.create((observer) => {
        return observer.next(MOCKS.PRODUCT_LIST_MOCK.filter(prod => prod.supplierId === supplierId.toString())).not.toBeNull();
      });
    });

    component.getProductsBySupplierId(supplierId);
    expect(component.products.length).toEqual(1);

  });

  it('should load no products if supplier doesnt exist', () => {
    let supplierId = 34;

    spyOn(component.productService, 'getProductBySupplierId').and.callFake(() => {
      return Observable.create((observer) => {
        return observer.next(MOCKS.PRODUCT_LIST_MOCK.filter(prod => prod.supplierId === supplierId.toString())).not.toBeNull();
      });
    });

    component.getProductsBySupplierId(supplierId);
    expect(component.products.length).toEqual(0);

  });

  it('should edit product', () => {

    spyOn(component.productService, 'updateProduct').and.callFake(() => {
      return Observable.create((observer) => {
        return observer.next(true);
      });
    });

    let getProductSpy = spyOn(component, 'getProducts');

    component.saveEdit();
    expect(getProductSpy).toHaveBeenCalled();

  });
});

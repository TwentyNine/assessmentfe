export class Product {
    Id: string;
    productId: string;
    productName: string;
    supplierId: string;
    categoryId: string;
    quantityPerUnit: string;
    unitPrice: number;
    unitsInStock: number;
    unitsInOrder: number;
    reorderLevel: number;
    discontinued: number;
}
